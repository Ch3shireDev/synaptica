﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProcessInputsScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public GameObject MutationRateInput;
    // Update is called once per frame
    private string lastText = "";

    void Update()
    {
        var inputField = MutationRateInput.GetComponent<InputField>();
        var mutationRateText = inputField.text;
        if (lastText == mutationRateText)
        {
            inputField.textComponent.color = Color.black;
            lastText = mutationRateText;
        }
        else
        {
            if (float.TryParse(mutationRateText, out float value))
            {
                GetComponent<MotherNatureScript>().Mutagen = value;
                inputField.textComponent.color = Color.green;

            }
            else
            {
                inputField.textComponent.color = Color.red;
            }
        }
    }
}
