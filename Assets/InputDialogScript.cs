﻿using UnityEngine;
using UnityEngine.UI;

public class InputDialogScript : MonoBehaviour
{
    public float Value;

    // Start is called before the first frame update
    private void Start()
    {
        ChangeText();
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void ChangeText()
    {
        var field = GetComponentInChildren<InputField>();
        var text = field.textComponent.text;
        var flag = float.TryParse(text, out var result);
        if (flag) Value = result;
    }
}