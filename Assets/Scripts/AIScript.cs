﻿
using UnityEngine;
using System.Collections.Generic;
public class AIScript : MonoBehaviour
{


    private float angle;

    public float angularVelocity = 100;

    public NeuralNetwork neural;

    public float Velocity = 1;

    private void Start()
    {
        if (neural == null)
        {
            neural = new NeuralNetwork(3, 5, 1);
        }

    }

    private int collisions = 0;

    void OnCollisionStay2D(Collision2D collider)
    {
        //collisions++;
    }

    private float[] GetDistances()
    {

        int layerMask = 1 << 9;
        layerMask = ~layerMask;

        var hit1 = Physics2D.Raycast(transform.position, transform.up,Mathf.Infinity,layerMask);
        var hit2 = Physics2D.Raycast(transform.position, transform.up + transform.right, Mathf.Infinity, layerMask);
        var hit3 = Physics2D.Raycast(transform.position, transform.up - transform.right, Mathf.Infinity, layerMask);
        return new[] {hit1.distance, hit2.distance, hit3.distance};
    }

    private void Update()
    {
        var physics = GetComponent<Rigidbody2D>();
        var inputFloats = GetDistances();
        var outputFloats = neural.ProcessVector(inputFloats);
        var outputAngle = 0.5f - outputFloats[0];
        var outputSpeed = 1;//0.5f - outputFloats[1];

        physics.velocity = transform.up * Velocity*outputSpeed;
        angle += outputAngle * angularVelocity * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0, 0, angle);

        var pos = transform.position;
        var x = (int)pos.x;
        int y = (int)pos.y;
        positions.Add(new Vector2Int(x, y));

    }

    public HashSet<Vector2Int> positions = new HashSet<Vector2Int>();


    public int Points => positions.Count;

    public void AddCoin()
    {
        //Coints++;
    }
}