﻿using UnityEngine;

public class Matrix
{
    private readonly int a;
    private readonly int b;
    private readonly float[,] values;

    private Matrix(int a, int b, float[,] values, float mutagen = 0)
    {
        this.a = a;
        this.b = b;
        this.values = new float[a, b];

        for (var i = 0; i < a; i++)
        for (var j = 0; j < b; j++)
            this.values[i, j] = values[i, j];

        var aa = Random.Range(0, a - 1);
        var bb = Random.Range(0, b - 1);

        //Debug.Log($"Changed {aa} {bb}");

        float u1 = 1.0f - Random.Range(0f, 1f);
        float u2 = 1.0f - Random.Range(0f, 1f);
        float randStdNormal = Mathf.Sqrt(-2.0f * Mathf.Log(u1)) *
                               Mathf.Sin(2.0f * Mathf.PI * u2); //random normal(0,1)
        float randNormal =mutagen * randStdNormal;

        this.values[aa, bb] += randNormal;
    }

    public Matrix(int a, int b)
    {
        this.a = a;
        this.b = b;
        values = new float[a, b];
        for (var i = 0; i < a; i++)
        for (var j = 0; j < b; j++)
            values[i, j] = Random.Range(-1.0f, 1.0f);
    }

    public Matrix GetMutant(float mutagen = 0)
    {
        return new Matrix(a, b, values, mutagen);
    }

    public static float[] operator *(Matrix mat, float[] inputVector)
    {
        var a = mat.a;
        var b = mat.b;

        var outputVector = new float[b];

        for (var i = 0; i < b; i++)
        {
            outputVector[i] = 0;
            for (var j = 0; j < a; j++)
            {
                var matValue = mat.values[j, i];
                outputVector[i] += matValue * inputVector[j];
            }
        }

        return outputVector;
    }
}

public class NeuralNetwork
{
    private Matrix LayerA;
    private Matrix LayerB;

    private NeuralNetwork()
    {
    }

    public NeuralNetwork(int inputNum, int hiddenNum, int outputNum)
    {
        LayerA = new Matrix(inputNum, hiddenNum);
        LayerB = new Matrix(hiddenNum, outputNum);
    }

    public NeuralNetwork(NeuralNetwork neural, float mutagen)
    {
        LayerA = neural.LayerA.GetMutant();
        LayerB = neural.LayerB.GetMutant();
    }

    public float[] ProcessVector(float[] inputFloats)
    {
        var hiddenLayer = LayerA * inputFloats;
        var outputLayer = LayerB * Sigmoid(hiddenLayer);

        return Sigmoid(outputLayer);
    }

    private float[] Sigmoid(float[] inputFloats)
    {
        var outputFloats = new float[inputFloats.Length];
        for (var i = 0; i < inputFloats.Length; i++) outputFloats[i] = 1 / (1 + Mathf.Exp(-inputFloats[i]));

        return outputFloats;
    }

    public NeuralNetwork GetMutant(float mutationRate = 0.05f)
    {
        var outputNeural = new NeuralNetwork
        {
            LayerA = LayerA.GetMutant(mutationRate),
            LayerB = LayerB.GetMutant(mutationRate)
        };
        return outputNeural;
    }
}