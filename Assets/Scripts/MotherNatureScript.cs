﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MotherNatureScript : MonoBehaviour
{
    public GameObject AngularField;
    private float AngularSpeed = 1;
    public int ChildrenNumber = 5;
    public GameObject CoinObject;

    public List<GameObject> Coins = new List<GameObject>();
    public GameObject CreatureObject;
    public List<GameObject> Creatures = new List<GameObject>();
    private int generation;
    public float GenerationTime = 20.0f;
    public GameObject GenTimeField;
    public float Mutagen = 0.1f;

    public GameObject MutagenField;

    private float oldTimeScale = 1;

    private float Speed = 1;
    public GameObject SpeedField;
    private Vector3 StartingPoint;
    public GameObject StartingPointGO;
    private float timer;
    public GameObject TimeScaleField;

    public GameObject ChildrenCountField;
    //private int childrenCount = 100;

    private void Start()
    {
        UpdateTime();
        UpdateValues();
        StartingPoint = StartingPointGO.transform.position;
        //Populate(new Vector3(0.5f, 0.5f));
        CreateCharacters(ChildrenNumber, null);
    }

    private void CreateCharacters(int number, GameObject Daddy)
    {
        NeuralNetwork neural = null;
        if (Daddy != null) neural = Daddy.GetComponent<AIScript>().neural;
        foreach (var element in Creatures) Destroy(element);
        Creatures = new List<GameObject>();

        for (var i = 0; i < number; i++)
        {
            var element = Instantiate(CreatureObject, StartingPoint, Quaternion.identity);
            var elementAI = element.GetComponent<AIScript>();
            if (neural != null) elementAI.neural = neural.GetMutant(Mutagen);
            elementAI.Velocity = Speed;
            elementAI.angularVelocity = AngularSpeed;
            Creatures.Add(element);
        }
    }

    private bool IsFreeSpace(Vector3 position)
    {
        var hit = Physics2D.Raycast(position, Vector3.forward);
        return hit.collider == null;
    }

    private void Populate(Vector3 startingPosition)
    {
        var set = new HashSet<Vector3>();
        if (IsFreeSpace(startingPosition)) set.Add(startingPosition);

        var oldCount = set.Count;
        var newCount = oldCount + 1;
        var deltaPositions = new[]
        {
            Vector3.up, Vector3.down, Vector3.left, Vector3.right
        };


        while (oldCount != newCount)
        {
            oldCount = newCount;
            var list = set.ToArray();
            foreach (var position in list)
            foreach (var deltaPos in deltaPositions)
            {
                var newPos = position + deltaPos;
                if (IsFreeSpace(position)) set.Add(newPos);
            }

            newCount = set.Count;
        }


        foreach (var position in set)
        {
            var coin = Instantiate(CoinObject, position, Quaternion.identity);
            Coins.Add(coin);
        }
    }

    private void UpdateTime()
    {
        if (TimeScaleField)
        {
            var inputDialog = TimeScaleField.GetComponent<InputDialogScript>();
            var timeScale = inputDialog.Value;
            Debug.Log(timeScale);
            if (timeScale > 0 && Math.Abs(oldTimeScale - timeScale) > 0.1f) Time.timeScale = timeScale;
            oldTimeScale = timeScale;
        }
    }

    private void UpdateValues()
    {
        if (MutagenField)
        {
            var mutagen = MutagenField.GetComponent<InputDialogScript>().Value;
            if (mutagen > 0) Mutagen = mutagen;
        }

        if (GenTimeField)
        {
            var inputDialog = GenTimeField.GetComponent<InputDialogScript>();
            var genTime = inputDialog.Value;
            if (genTime > 0) GenerationTime = genTime;
        }

        if (SpeedField)
        {
            var inputDialog = SpeedField.GetComponent<InputDialogScript>();
            var speedValue = inputDialog.Value;
            if (speedValue > 0) Speed = speedValue;
        }

        if (AngularField)
        {
            var inputDialog = AngularField.GetComponent<InputDialogScript>();
            var angularValue = inputDialog.Value;
            if (angularValue > 0) AngularSpeed = angularValue;
        }

        if (ChildrenCountField)
        {
            var inputDialog = ChildrenCountField.GetComponent<InputDialogScript>();
            var value = inputDialog.Value;
            if (value >= 1) ChildrenNumber = (int) value;
        }
    }

    private void Update()
    {
        timer += Time.deltaTime;
        UpdateTime();
        if (!(timer > GenerationTime)) return;
        UpdateValues();
        timer = 0;
        //Populate(StartingPoint);
        var Daddy = GetBestCreature();
        CreateCharacters(ChildrenNumber, Daddy);
        generation++;
        GameObject.Find("GenerationText").GetComponent<Text>().text = $"Generation: {generation}";
    }

    private GameObject GetBestCreature()
    {
        var bestScore = 0f;
        GameObject bestObject = null;
        foreach (var creature in Creatures)
        {
            var n = creature.GetComponent<AIScript>().Points;
            if (n <= bestScore) continue;
            bestScore = n;
            bestObject = creature;
        }

        var score = $"Best score: {bestScore}";
        Debug.Log(score);
        GameObject.Find("BestScoreText").GetComponent<Text>().text = score;
        return bestObject;
    }
}